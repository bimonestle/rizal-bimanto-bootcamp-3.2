<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UserList;
use JWTAuth;

class BookingController extends Controller
{
    function getRoom(){
        $roomList = RoomList::get();
        return response()->json($roomList, 200);
    }

    function bookRoom(Request $request){
        DB::beginTransaction();
        try{
            $customer = $request->input('CustomerID');
            $room = $request->input('KamarID');

            $odr = new Order;
            $odr->customer = $customer;
            $odr->room = $room;
            $odr->save(); 

            $kamarlist = RoomList::get();

            DB::commit();
            return response()->json($kamarlist, 200);           
        }

        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()],500);
        }
    }
}
