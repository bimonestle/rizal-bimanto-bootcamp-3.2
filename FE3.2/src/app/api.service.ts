import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
    getroomList(){
      let token = localStorage.getItem("token");
      let headers = new Headers({ "Authorization" : "Bearer " + ""});
      let options = new RequestOptions({ headers: headers });

      console.log(token);

      return this.http.get('http://localhost:8000/api/roomlist?token=' + token)
      .map(result => result.json());
    }

}
