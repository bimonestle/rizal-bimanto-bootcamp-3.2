import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-roomlist',
  templateUrl: './roomlist.component.html',
  styleUrls: ['./roomlist.component.css']
})
export class RoomlistComponent implements OnInit {

  constructor(private api:ApiService) { }

  tipe:string = "";
  price:decimal = "";
  status:string = "";

  roomlist:object[];

  ngOnInit() {
    this.api.validateRoom();
    this.api.getroomlist()
            .subscribe(result => this.roomlist = result);
  }

  book(){
    this.api.book(this.customer, this.room)
  }

  // addUser(){
  //   this.api.addUser(this.name, this.email, this.address)
  //           .subscribe(result => this.roomlist = result);

  //   this.name = "",
  //   this.email = "",
  //   this.address = ""           
  // }

  // removeUser(id:number){
  //   this.api.removeUser(id)
  //           .subscribe(result => this.roomlist = result);
  // }
  
  // open(){
  //   jQuery("#dialog").dialog("open");
  //   // $("#dialog").dialog("open"); //$ disesuaikan dengan import jquery nya
  // }

}
