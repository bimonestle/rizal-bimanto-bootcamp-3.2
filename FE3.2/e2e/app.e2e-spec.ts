import { FE3.2Page } from './app.po';

describe('fe3.2 App', () => {
  let page: FE3.2Page;

  beforeEach(() => {
    page = new FE3.2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
